#define _DEFAULT_SOURCE
#include <dirent.h> 
#include <stdio.h>
#include <unistd.h> 
#include <errno.h>
#include <sys/stat.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
int max;
pthread_mutex_t lock;
FILE * logs;


void *accessDir(void *arg) {
    //add log for accessing dir
    char *dirr = (char *) arg;
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    char buffer[20];
    char logg[1000];
    strftime(buffer, sizeof(buffer), "%d-%m-%Y %H:%M:%S", &tm);
    sprintf(logg, "%s ACCESSED %s\n", buffer, dirr);
    fputs(logg, logs);

    //open the dir and read the content inside it
    DIR *d;
    struct dirent *dir;
    d = opendir(dirr);
    if (d) {
        while ((dir = readdir(d)) != NULL) {
            //differentiate between folder and file
            if (dir->d_type == DT_REG) {        //file
                //open extensions.txt and compare the file with the extensions
                FILE *fp;
                size_t len = 0;
                ssize_t read;
                fp = fopen("hehe/extensions.txt", "r");
                if (fp == NULL){
                    exit(EXIT_FAILURE);
                }  
                char line[1000];    
                while (fgets(line, sizeof(line), fp)) {
                    line[strcspn(line, "\r\n")] = '\0';
                    if(strstr(dir->d_name,line)!=NULL){ 
                        //if match then start moving the file 
                        char command[10000];
                        int foldcount=2;
                        int moved=1;

                        //loop for checking if the folder for the ext already full
                        while(moved){
                            char foldname[10000];
                            if(moved==1){
                                sprintf(foldname, "hehe/categorized/%s", line);      
                            }else{
                                sprintf(foldname, "hehe/categorized/%s_%d_", line, moved);    
                            }
                            DIR* catego = opendir(foldname);
                            if (catego){
                                //check if current dir full
                                int file_count = 0;
                                struct dirent * entry;
                                while ((entry = readdir(catego)) != NULL) {
                                    if (entry->d_type == DT_REG) { 
                                        file_count++;
                                    }
                                }
                                if(file_count<max){
                                    //move the file
                                    sprintf(command, "mv \'%s/%s\' %s", dirr,dir->d_name, foldname);
                                    int status = system(command);
                                    if(status==0){
                                        time_t t = time(NULL);
                                        struct tm tm = *localtime(&t);
                                        char buffer[20];
                                        char logg[1000];
                                        strftime(buffer, sizeof(buffer), "%d-%m-%Y %H:%M:%S", &tm);
                                        sprintf(logg, "%s MOVED %s file : \'%s/%s\' > %s\n", buffer, line, dirr,dir->d_name, foldname);
                                        fputs(logg, logs);
                                        
                                    }
                                    
                                    moved=0;
                                    closedir(catego);
                                    break;
                                }else{
                                    //next directory
                                    moved++;
                                    closedir(catego);
                                }
                            } else{
                                //make directory if not exist
                                sprintf(command, "mkdir %s", foldname);
                                int status=system(command);
                                if(status==0){
                                    time_t t = time(NULL);
                                    struct tm tm = *localtime(&t);
                                    char buffer[20];
                                    char logg[1000];
                                    strftime(buffer, sizeof(buffer), "%d-%m-%Y %H:%M:%S", &tm);
                                    sprintf(logg, "%s MAKED %s\n", buffer, foldname);
                                    fputs(logg, logs);
                                }
                                //move the file
                                sprintf(command, "mv \'%s/%s\' %s", dirr,dir->d_name, foldname);
                                status = system(command);
                                if(status==0){
                                    time_t t = time(NULL);
                                    struct tm tm = *localtime(&t);
                                    char buffer[20];
                                    char logg[1000];
                                    strftime(buffer, sizeof(buffer), "%d-%m-%Y %H:%M:%S", &tm);
                                    sprintf(logg, "%s MOVED %s file : \'%s/%s\' > %s\n", buffer,line, dirr,dir->d_name, foldname);
                                    fputs(logg, logs);
                                    
                                }
                                moved=0;
                                closedir(catego);
                                break;
                            }
                        }
                        break;
                    }    
                }
                //if not match any extension
                //make dir other and move it there
                DIR *catego = opendir("hehe/categorized/other");
                if(!catego){
                    system("mkdir hehe/categorized/other");
                    time_t t = time(NULL);
                    struct tm tm = *localtime(&t);
                    char buffer[20];
                    char logg[1000];
                    strftime(buffer, sizeof(buffer), "%d-%m-%Y %H:%M:%S", &tm);
                    sprintf(logg, "%s MAKED hehe/categorized/other\n", buffer);
                    fputs(logg, logs);
                }
                char command[1000];
                sprintf(command, "mv \'%s/%s\' hehe/categorized/other", dirr,dir->d_name);
                int status = system(command);
                if(status==0){
                    time_t t = time(NULL);
                    struct tm tm = *localtime(&t);
                    char buffer[20];
                    char logg[1000];
                    strftime(buffer, sizeof(buffer), "%d-%m-%Y %H:%M:%S", &tm);
                    sprintf(logg, "%s MOVED other file : \'%s/%s\' > hehe/categorized/other\n", buffer, dirr,dir->d_name);
                    fputs(logg, logs);
                    
                }
                closedir(catego);
                fclose(fp);
            } else if (dir->d_type == DT_DIR) { //folder
                //create a new thread for the folder
                if (strcmp(dir->d_name, ".") == 0 || strcmp(dir->d_name, "..") == 0) {
                    continue;
                }
                pthread_t thread;
                char *subdir[1000];
                sprintf(subdir, "%s/%s", dirr,dir->d_name);
                pthread_create(&thread, NULL, accessDir, (void *) subdir);
                pthread_join(thread, NULL);
            }
        }
        closedir(d);
    }
    return NULL;
}


int main(void) {
    //make log.txt and open it for write
    system("touch hehe/log.txt");
    logs = fopen("hehe/log.txt", "w+");

    //make categorized dir and add log
    system("mkdir hehe/categorized");
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    char buffer[20];
    char logg[1000];
    strftime(buffer, sizeof(buffer), "%d-%m-%Y %H:%M:%S", &tm);
    sprintf(logg, "%s MAKED hehe/categorized\n", buffer);
    fputs(logg, logs);

    //get the max amount from max.txt
    FILE *fp;
    char *line = NULL;
    size_t len = 0;
    ssize_t read;
    fp = fopen("hehe/max.txt", "r");
    if (fp == NULL){
        printf("Unable to open \n");
        exit(EXIT_FAILURE);
    }      
    while ((read = getline(&line, &len, fp)) != -1) {
        max=atoi(line);
    }
    fclose(fp);

    //create first thread for accessing hehe/files
    pthread_t thread;
    char *dir = "hehe/files";
    pthread_create(&thread, NULL, accessDir, (void *) dir);
    pthread_join(thread, NULL);
    return 0;
}
