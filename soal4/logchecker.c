#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>

int main(){
    int acc;
    FILE *fp;
    fp = fopen("hehe/log.txt", "r");
    if (fp == NULL){
        exit(EXIT_FAILURE);
    }  
    char line[1000];
    int i=0;    
    while (fgets(line, sizeof(line), fp)){
        if(strstr(line,"ACCESSED")!=NULL){
            acc++;
        }
    }

    printf("Accessed folder: %d\n", acc);
}