#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

int main(){
    pid_t c_pid=fork();
    if(c_pid==0){
        char *argv[]={"wget", "-O", "hehe.zip", "https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download", NULL};
        execv("/bin/wget",argv);
    } else
    {
        wait(NULL);
    }
    
    c_pid=fork();
    if(c_pid==0){
        char *argv[]={"unzip", "hehe.zip", "-d", "hehe", NULL};
        execv("/bin/unzip", argv);
    } else
    {
        wait(NULL);
    }
}