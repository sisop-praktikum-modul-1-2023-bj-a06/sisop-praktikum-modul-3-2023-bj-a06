#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define rows1 4
#define cols2 5
#define SHM_KEY(i, j) (key + (i)*cols2 + (j))

int main(){
    key_t key=1234;
    int factres[rows1][cols2];
    int *res;
    int shmid[rows1][cols2];

    for(int i=0; i<rows1;i++){
        for(int j=0;j<cols2;j++){
            shmid[i][j] = shmget(SHM_KEY(i, j), sizeof(int), 0666);
            if (shmid[i][j] == -1) {
                perror("shmget");
                exit(1);
            }
            int *res = shmat(shmid[i][j], NULL, 0);
            if (res == (int*)-1) {
                perror("shmat");
                exit(1);
            }
            int *angka = res;
            factres[i][j] = *res;
            
            printf("%d ", *res);
            if (shmdt(res) == -1) {
                perror("shmdt");
                exit(1);
            }
        }
        puts("");
    }
    puts("");
    for(int i=0; i<rows1;i++){
        for(int j=0;j<cols2;j++){
            long long akhir=1;
            for(int k=1;k<=factres[i][j];k++){
                akhir *=k;
            }
            printf("%lld ", akhir);
        }
        puts("");
    }
}