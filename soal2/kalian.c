#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>

#define rows1 4
#define cols1 2
#define rows2 2
#define cols2 5

int main(){

    key_t key=1234;
    int matriks1[rows1][cols1], matriks2[rows2][cols2];
    int shmid[rows1][cols2];
    
    srand(time(0));
    for(int i=0; i<rows1;i++){
        for(int j=0;j<cols1;j++){
            matriks1[i][j]= rand() % 5 + 1;
        }
    }
    for(int i=0; i<rows1;i++){
        for(int j=0;j<cols1;j++){
            printf("%d ", matriks1[i][j]);
        }
        puts("");
    }
    puts("");
    for(int i=0; i<rows2;i++){
        for(int j=0;j<cols2;j++){
            matriks2[i][j]= rand() % 4 + 1;
        }
    }
    for(int i=0; i<rows2;i++){
        for(int j=0;j<cols2;j++){
            printf("%d ", matriks2[i][j]);
        }
        puts("");
    }
    puts("");
    for(int i=0; i<rows1;i++){
        for(int j=0;j<cols2;j++){
            shmid[i][j] = shmget(key+i*cols2+j, sizeof(int), IPC_CREAT | 0666);
            int *res = shmat(shmid[i][j], NULL, 0);
            *res=0;
            for(int k=0;k<cols1;k++){
                *res += matriks1[i][k] * matriks2[k][j];
            }
        }
    }

    for(int i=0; i<rows1;i++){
        for(int j=0;j<cols2;j++){
            printf("%d ", *((int*)shmat(shmid[i][j], NULL, 0)));
            
        }
        puts("");
    }
    sleep(5);
    for(int i=0; i<rows1;i++){
        for(int j=0;j<cols2;j++){
            shmdt(shmat(shmid[i][j], NULL, 0));
            shmctl(shmid[i][j], IPC_RMID, NULL);
        }
    }
}