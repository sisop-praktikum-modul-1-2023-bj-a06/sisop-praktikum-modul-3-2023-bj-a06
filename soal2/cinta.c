#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>


#define rows1 4
#define cols2 5
#define SHM_KEY(i, j) (key + (i)*cols2 + (j))

int globres[rows1*cols2 +1];
long long factres[rows1*cols2 + 1]; 

void *run(void *args){
    int angka;
    long long factorial=1;
    angka = (*(int*)args);
    printf("%d ", angka);
    for(int i=2; i<=globres[angka];i++){
        factorial*=i;
    }
    factres[angka]=factorial;
    //printf("%lld ", factres[angka]);
}

int main(){
    key_t key=1234;
    int *res;
    int shmid[rows1][cols2];

    int threadnum= rows1 * cols2 + 1;
    pthread_t t_id[threadnum];
    int *num = (int*)malloc(sizeof(int));
    int x=0;
    for(int i=0; i<rows1;i++){
        for(int j=0;j<cols2;j++){
            *num = x;
            shmid[i][j] = shmget(SHM_KEY(i, j), sizeof(int), 0666);
            if (shmid[i][j] == -1) {
                perror("shmget");
                exit(1);
            }
            int *res = shmat(shmid[i][j], NULL, 0);
            if (res == (int*)-1) {
                perror("shmat");
                exit(1);
            }
            
            //printf("%d ", *num);
            globres[*num] = *res;
            //printf("%d ", globres[*num]);
            pthread_create(&t_id[*num], NULL, &run, (void*)num);

            printf("%d ", *res);
            if (shmdt(res) == -1) {
                perror("shmdt");
                exit(1);
            }
            x++;
        }
        puts("");
    }

    for(int i=0;i<rows1*cols2;i++){
        pthread_join(t_id[i], NULL);
    }
    for(int i=0;i<rows1*cols2;i++){
        printf("%lld ", factres[i]);
    }
}