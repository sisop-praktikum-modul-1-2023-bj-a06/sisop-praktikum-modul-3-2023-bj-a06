# sisop-praktikum-modul-3-2023-BJ-A06

## Laporan Pengerjaan Soal Shift Modul 2 Praktikum Sistem Operasi

### Daftar Anggota :
1. Lihardo Marson Purba - 5025211238
2. Farhan Dwi Putra     - 5025211093
3. Victor Gustinova     - 5025211159

### Soal 1
#### Kendala dan Catatan Soal 1
Untuk soal nomor 1 ini kami memohon maaf tidak dapat membuatnya. Kami mencoba menyelesaikan soal lain terlebih dahulu akan tetapi soal lain tersebut belum bisa selesai secar sempurna. Kamipun akhirnya kehabisan waktu. Mohon maaf atas ketidaktepatan kami.
### Soal 2
- Pertama dibuat program untuk membuat matriks yang diminta di kalian.c. Program ini menggunakan time untuk merandomisasi isi dari matriks. Program ini juga sudah siap untuk melakukan shared memory dan dapat menghitung hasil perkalian matriks. Karena perlu berbagai nilai, maka key dari shared memory akan berbeda beda. Berikut merupakan bagian utama dari program tersebut dimana dilakukan shared memory dan perhitungan perkalian.
```sh
for(int i=0; i<rows1;i++){
        for(int j=0;j<cols2;j++){
            shmid[i][j] = shmget(key+i*cols2+j, sizeof(int), IPC_CREAT | 0666);
            int *res = shmat(shmid[i][j], NULL, 0);
            *res=0;
            for(int k=0;k<cols1;k++){
                *res += matriks1[i][k] * matriks2[k][j];
            }
        }
    }

```
- Selanjutnya program cinta.c akan juga menampilkan hasil perkalian yang sama dengan program sebelumnya. Seharusnya, program cinta.c akan menghitung nilai faktorial dari nilai matriks, namun kami masih belum bisa melakukannay secara multithread. Pada code dibawah dilakukan shared memory, penampilan nilai, dan pembuatan thread (yang gagal).
```sh
for(int i=0; i<rows1;i++){
        for(int j=0;j<cols2;j++){
            *num = x;
            shmid[i][j] = shmget(SHM_KEY(i, j), sizeof(int), 0666);
            if (shmid[i][j] == -1) {
                perror("shmget");
                exit(1);
            }
            int *res = shmat(shmid[i][j], NULL, 0);
            if (res == (int*)-1) {
                perror("shmat");
                exit(1);
            }
            
            //printf("%d ", *num);
            globres[*num] = *res;
            //printf("%d ", globres[*num]);
            pthread_create(&t_id[*num], NULL, &run, (void*)num);

            printf("%d ", *res);
            if (shmdt(res) == -1) {
                perror("shmdt");
                exit(1);
            }
            x++;
        }
        puts("");
    }

``` 
- Pada program ketiga akan dilakukan hal yang sama dengan cinta.c namun perhitungan faktorial dilakukan tanpa thread. Program ini dapat melakukan semua yang diminta. Metode shared memory menggunakan metode ytang sama dengan cinta.c sedangkan metode faktorialnya adalah sebagai berikut.
```sh
for(int i=0; i<rows1;i++){
        for(int j=0;j<cols2;j++){
            long long akhir=1;
            for(int k=1;k<=factres[i][j];k++){
                akhir *=k;
            }
            printf("%lld ", akhir);
        }
        puts("");
    }
```
#### Kendala dan Catatan Soal 2
Soal nomor 2 memiliki kendala pada melakukan multithread. Entah mengapa, setiap kali thread dibuat maka terbuat lebih dari satu thread. Argumen dari thread tersebut juga menjadi tidak pas seperti 0 atau tidak terurut. Hal inilah yang membuat kami tidak bisa menyelesaikan soal dengan sempurna.

### Soal 3
Pada soal 3 secara garis besar kita diminta untuk melakukan decrypt terhadap file song-playlist.json ke file playlist.txt serta mengurutkannya secara alfabet. Dalam prosesnya ada 2 program user.c dan stream.c dimana user.c program untuk menerima input user dan memasukkan ke dalam message queue untuk diproses dan menjalankan program yang sesuai dengan inputan user.

sejauh ini saya masih menyelesaikan decrypt terhadap jenis rot13 yang dapat dilihat di bawah ini.

```sh
void *decrypt_song() {
  //creating temporary file to decrypt  
  system("touch rot13.txt");
  system("touch base64.txt");
  system("touch hex.txt");
  
  //copy-paste every song that clasified by their method
  system("jq '.[] | select(.method == \"rot13\") | .song' song-playlist.json > rot13.txt");
  system("jq '.[] | select(.method == \"base64\") | .song' song-playlist.json > base64.txt");
  system("jq '.[] | select(.method == \"hex\") | .song' song-playlist.json > hex.txt");
  
  //delete all double quote from temporary text file
  system ("sed -i 's/\"//g' rot13.txt");
  system ("sed -i 's/\"//g' base64.txt");
  system ("sed -i 's/\"//g' hex.txt");
  
  pid_t child_1, child_2,child_3;
  
  //decrypt all song that have method rot13
  child_1 = fork();
  if (child_1 == 0) {
    char *args[] = {"sed", "-i", "y/abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/nopqrstuvwxyzabcdefghijklmNOPQRSTUVWXYZABCDEFGHIJKLM/", "rot13.txt", NULL};
    execv("/usr/bin/sed", args);
  }
  else {
    wait(NULL);
    system ("cp rot13.txt playlist.txt");
    char *args[] = {"rm", "rot13.txt"};
    execv("/usr/bin/rm", args);
  }
  
  child_2 = fork ();
if (child_2 == 0) {
    // open base64.txt and playlist.txt files
    FILE *base64_fp = fopen("base64.txt", "r");
    FILE *playlist_fp = fopen("playlist.txt", "a");

    // read each line from base64.txt, decode it, and write it to playlist.txt
    char line[1024];
    while (fgets(line, sizeof(line), base64_fp)) {
        // remove trailing newline character
        line[strcspn(line, "\n")] = 0;
        // decode the line using the base64 command-line tool and write the decoded text to playlist.txt
        FILE *base64_decoder = popen("base64 -d", "w");
        fprintf(base64_decoder, "%s\n", line);
        fflush(base64_decoder);
        char decoded_line[1024];
        fgets(decoded_line, sizeof(decoded_line), base64_decoder);
        pclose(base64_decoder);
        fprintf(playlist_fp, "%s", decoded_line);
    }

    // close files
    fclose(base64_fp);
    fclose(playlist_fp);
}
else {
    // remove base64.txt
    //wait(NULL);
    char *args[] = {"rm", "base64.txt"};
    execv("/usr/bin/rm", args);
}
  return NULL;
}

```
#### Kendala dan Catatan Soal 
Penerapan message queue dimana terdapat pengkondisian string dari message queue ke stream.c dari user.c. disamping itu, dalam proses decrypt saya masih belum menemukan cara untuk melakukan decrypt terhadap base64 dan hex yang menggunakan tool. saya belum bisa menuliskan script yang sesuai untuk melakukan decrypt menggunakan tool.
### Soal 4
- Pertama kita diminta untuk membuat program untuk mendownload dan mengunzip file pada link. Untuk melakukan hal tersebut kami menggunakan child dan fork untuk melaksanakan wget dan unzip.

```sh
int main(){
    pid_t c_pid=fork();
    if(c_pid==0){
        char *argv[]={"wget", "-O", "hehe.zip", "https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download", NULL};
        execv("/bin/wget",argv);
    } else
    {
        wait(NULL);
    }
    
    c_pid=fork();
    if(c_pid==0){
        char *argv[]={"unzip", "hehe.zip", "-d", "hehe", NULL};
        execv("/bin/unzip", argv);
    } else
    {
        wait(NULL);
    }

```
- Setelah itu dibuat program categorize.c untuk mengkategorisasi file. Pertama akan dibuat dan dibuka file log.txt untuk pengeditan isinya. Berikut merupakan pembuatan log dimana menggunakan sprintf dan fputs.
```sh
//make log.txt and open it for write
    system("touch hehe/log.txt");
    logs = fopen("hehe/log.txt", "w+");

    //make categorized dir and add log
    system("mkdir hehe/categorized");
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    char buffer[20];
    char logg[1000];
    strftime(buffer, sizeof(buffer), "%d-%m-%Y %H:%M:%S", &tm);
    sprintf(logg, "%s MAKED hehe/categorized\n", buffer);
    fputs(logg, logs);
```

- Untuk mendapatkan banyak file maksimal dalam suatu folder maka diakses max.txt dan diambil nilainya.
```sh
FILE *fp;
    char *line = NULL;
    size_t len = 0;
    ssize_t read;
    fp = fopen("hehe/max.txt", "r");
    if (fp == NULL){
        printf("Unable to open \n");
        exit(EXIT_FAILURE);
    }      
    while ((read = getline(&line, &len, fp)) != -1) {
        max=atoi(line);
    }
    fclose(fp);
```
- Selanjutnya dilakukan pembuatan thread untuk mengakses direktori hehe/files.
```
    pthread_t thread;
    char *dir = "hehe/files";
    pthread_create(&thread, NULL, accessDir, (void *) dir);
    pthread_join(thread, NULL);
    return 0;
```
- Untuk setiap thread akan dilakukan berbagai proses. Karena bagian ini cukup panjang maka saya akan menuliskan algoritma umumnya saja. Pertama dilakukan pengaksesan dir dan dibuat log.  Lalu diambil list dari setiap isi direktori tersebut dan dibagi menjadi file atau folder. Jika file akan dilakukan pemindahan dan jika folder akan dilakukan pembuatan thread baru. 

- Logchecker hanya dapat dibuat dalam bagian menghitung accessed saja. Pada program ini akan dibuka file log.txt dan dihitung berapa banyak line yang memiliki tulisan ACCESSED.

#### Kendala dan Catatan Soal 4
Soal 4 tidak dapat diselesaikan dengan sempurna. Pada poin c serta subpoin 2 dan 3 dari poin f. Kami bingung dalam menyelesaikan poin tersebut karena perlu dilakukan pengurutan. Pada akhirnya, kami kekurangan waktu dan tidak dapat menyelesaikan poin-poin tersebut.
