#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <errno.h>
#include <string.h>
#include <jansson.h>
#include <sys/wait.h>
#define MAX_LINE_LENGTH 4000

void *decrypt_song() {
  //creating temporary file to decrypt  
  system("touch rot13.txt");
  system("touch base64.txt");
  system("touch hex.txt");
  
  //copy-paste every song that clasified by their method
  system("jq '.[] | select(.method == \"rot13\") | .song' song-playlist.json > rot13.txt");
  system("jq '.[] | select(.method == \"base64\") | .song' song-playlist.json > base64.txt");
  system("jq '.[] | select(.method == \"hex\") | .song' song-playlist.json > hex.txt");
  
  //delete all double quote from temporary text file
  system ("sed -i 's/\"//g' rot13.txt");
  system ("sed -i 's/\"//g' base64.txt");
  system ("sed -i 's/\"//g' hex.txt");
  
  pid_t child_1, child_2,child_3;
  
  //decrypt all song that have method rot13
  child_1 = fork();
  if (child_1 == 0) {
    char *args[] = {"sed", "-i", "y/abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/nopqrstuvwxyzabcdefghijklmNOPQRSTUVWXYZABCDEFGHIJKLM/", "rot13.txt", NULL};
    execv("/usr/bin/sed", args);
  }
  else {
    wait(NULL);
    system ("cp rot13.txt playlist.txt");
    char *args[] = {"rm", "rot13.txt"};
    execv("/usr/bin/rm", args);
  }
  
  child_2 = fork ();
if (child_2 == 0) {
    // open base64.txt and playlist.txt files
    FILE *base64_fp = fopen("base64.txt", "r");
    FILE *playlist_fp = fopen("playlist.txt", "a");

    // read each line from base64.txt, decode it, and write it to playlist.txt
    char line[1024];
    while (fgets(line, sizeof(line), base64_fp)) {
        // remove trailing newline character
        line[strcspn(line, "\n")] = 0;
        // decode the line using the base64 command-line tool and write the decoded text to playlist.txt
        FILE *base64_decoder = popen("base64 -d", "w");
        fprintf(base64_decoder, "%s\n", line);
        fflush(base64_decoder);
        char decoded_line[1024];
        fgets(decoded_line, sizeof(decoded_line), base64_decoder);
        pclose(base64_decoder);
        fprintf(playlist_fp, "%s", decoded_line);
    }

    // close files
    fclose(base64_fp);
    fclose(playlist_fp);
}
else {
    // remove base64.txt
    //wait(NULL);
    char *args[] = {"rm", "base64.txt"};
    execv("/usr/bin/rm", args);
}
  return NULL;
}

int main (){
  pthread_t tid;
  
  // Create a new thread and execute decrypt_song() within it
  if (pthread_create(&tid, NULL, decrypt_song, NULL) != 0) {
    fprintf(stderr, "Error creating thread\n");
    return 1;
  }
  
  // Wait for the thread to finish
  if (pthread_join(tid, NULL) != 0) {
    fprintf(stderr, "Error joining thread\n");
    return 1;
  }
  
  return 0;
}
